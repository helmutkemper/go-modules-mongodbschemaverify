package mongoschema

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
	"time"
)

func Test_DateIncorrectDateAsString(t *testing.T) {
	var err error
	var errorList = make([]error, 0)
	var jSchema = JsonSchema{}

	var schemaInTest = schemaDate()
	err = jSchema.UnmarshalJSON(schemaInTest)
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}

	mongoData := map[string]interface{}{
		"_id":  primitive.ObjectID([12]byte{0x5f, 0x49, 0xa1, 0x33, 0xa8, 0xf1, 0x30, 0x21, 0x42, 0xba, 0x60, 0x69}),
		"DATE": time.Now().String(),
	}
	errorList = jSchema.Verify(mongoData["_id"].(primitive.ObjectID), mongoData)
	if len(errorList) != 1 && errorList[0].Error() != "document _id: 'ObjectID(\"5f49a133a8f1302142ba6069\")': the value `2020-09-17 17:39:04.3775786 -0300 -03 m=+0.021996501` (type string  in key `DATE`) must be date" {
		for _, err = range errorList {
			t.Error(err)
		}
		t.Fail()
		return
	}
}
