package mongoschema

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Verify (English): Check the key containing data from MongoDB
//   ID: MongoDB document ID
//   Data: MongoDB document
//
// Verify (Português): Verifica a chave contendo dado vindo do MongoDB
//   ID: MongoDB documento ID
//   Data: documento do MongoDB
func (el *JsonSchema) Verify(
	ID primitive.ObjectID,
	data map[string]interface{},
) (
	errorList []error,
) {
	return el.verifySubData(ID, "", data)
}
