package mongoschema

import "io/ioutil"

// LoadAndUnmarshalJsonSchema (English): Load the file containing the MongoDB schema
//
// LoadAndUnmarshalJsonSchema (Português): Carrega o arquivo contendo o esquema do
// MongoDB
func (el *JsonSchema) LoadAndUnmarshalJsonSchema(filePath string) (err error) {
	var fileData []byte

	fileData, err = ioutil.ReadFile(filePath)
	if err != nil {
		return
	}

	err = el.UnmarshalJSON(fileData)
	return
}
