package mongoschema

func schemaObject() (schema []byte) {
	return []byte(`{
   "validator": {
      "$jsonSchema": {
         "bsonType": "object",
         "required": [ "address" ],
         "properties": {
            "address": {
               "bsonType": "object",
               "required": [ "city", "street" ],
               "properties": {
                  "street": {
                     "bsonType": "object",
                     "required": [ "name", "number" ],
                     "properties": {
                        "name": {
                           "bsonType": "string",
                           "description": "street name"
                        },
                        "number": {
                           "bsonType": "int",
                           "description": "house number"
                        }
                     }
                  },
                  "city": {
                     "bsonType": "string",
                     "description": "must be a string and is required"
                  }
               }
            }
         }
      }
   }
}`)
}
