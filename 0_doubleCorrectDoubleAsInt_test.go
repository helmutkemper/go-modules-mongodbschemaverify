package mongoschema

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
)

func Test_DoubleCorrectDoubleAsInt(t *testing.T) {
	var err error
	var errorList = make([]error, 0)
	var jSchema = JsonSchema{}
	jSchema.AcceptsIntegerAsDouble()

	var schemaInTest = schemaDouble()
	err = jSchema.UnmarshalJSON(schemaInTest)
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}

	mongoData := map[string]interface{}{
		"_id":    primitive.ObjectID([12]byte{0x5f, 0x49, 0xa1, 0x33, 0xa8, 0xf1, 0x30, 0x21, 0x42, 0xba, 0x60, 0x69}),
		"DOUBLE": 31415,
	}
	errorList = jSchema.Verify(mongoData["_id"].(primitive.ObjectID), mongoData)
	if len(errorList) != 0 {
		t.Fail()
		return
	}
}
