package mongoschema

// processProperties (English): Process each line of the 'sub schema'
//
// processProperties (Português): Processa cada linha do 'sub schema'
func (el *JsonSchema) processProperties(prefix string, properties map[string]interface{}) {
	for fieldName, fieldProperties := range properties {
		el.getJSonSchema(prefix, fieldName, fieldProperties.(map[string]interface{}))
	}
}
