package mongoschema

func (el *JsonSchema) _processRequiredFields(prefix string, schema map[string]interface{}) {

	var tmpFieldName string

	if len(el.RequiredList) == 0 {
		el.RequiredList = make(map[string]bool)
	}

	var requiredListTmp = make([]interface{}, 0)
	requiredListTmp, _ = schema[kJsonSchemaRequired].([]interface{})
	for _, fieldName := range requiredListTmp {
		tmpFieldName = fieldName.(string)
		if prefix != "" {
			tmpFieldName = prefix + "." + tmpFieldName
		}
		el.RequiredList[tmpFieldName] = true
	}
}
