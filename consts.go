package mongoschema

const (
	kJsonSchemaValidator   = "validator"
	kJsonSchemaJsonSchema  = "$jsonSchema"
	kJsonSchemaBsonType    = "bsonType"
	kJsonSchemaEnum        = "enum"
	kJsonSchemaDescription = "description"
	//kJsonSchemaAdditionalProperties = "additionalProperties"
	kJsonSchemaRequired   = "required"
	kJsonSchemaProperties = "properties"
	kJsonSchemaMinimum    = "minimum"
	kJsonSchemaMaximum    = "maximum"
)
