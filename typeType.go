package mongoschema

type Type int

func (el Type) String() string {
	var typeAsString = [...]string{
		"",
		"object",
		"array",
		"number",
		"boolean",
		"string",
		"null",
	}

	return typeAsString[el]
}

func (el Type) FromAlias(alias string) (Type Type) {
	switch alias {
	case "object":
		Type = KTypeObject
	case "array":
		Type = KTypeArray
	case "number":
		Type = KTypeNumber
	case "boolean":
		Type = KTypeBoolean
	case "string":
		Type = KTypeString
	case "null":
		Type = KTypeNull
	}

	return
}

const (
	KTypeObject = iota + 1
	KTypeArray
	KTypeNumber
	KTypeBoolean
	KTypeString
	KTypeNull
)
