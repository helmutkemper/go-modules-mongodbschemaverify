package mongoschema

// getJSonMainSchema (English): Search for the '$jsonSchema' key in the MongoDB schema
//
// getJSonMainSchema (Português): Procura pela chave '$jsonSchema' dentro do esquema do
// MongoDB
func (el *JsonSchema) getJSonMainSchema(data interface{}) (schemaData map[string]interface{}, err error) {
	var found bool

	schemaData, err = el.verifyType(data)
	if err != nil {
		return
	}

	schemaData, found = schemaData[kJsonSchemaValidator].(map[string]interface{})
	if found == true {
		schemaData, err = el.verifyType(schemaData)
		if err != nil {
			return
		}
	}

	schemaData, found = schemaData[kJsonSchemaJsonSchema].(map[string]interface{})
	if found == true {
		schemaData, err = el.verifyType(schemaData)
		if err != nil {
			return
		}
	}

	return
}
