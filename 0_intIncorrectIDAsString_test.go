package mongoschema

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
)

func Test_IntIncorrectIDAsString(t *testing.T) {
	var err error
	var errorList = make([]error, 0)
	var jSchema = JsonSchema{}

	var schemaInTest = schemaInt()
	err = jSchema.UnmarshalJSON(schemaInTest)
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}

	mongoData := map[string]interface{}{
		"_id": primitive.ObjectID([12]byte{0x5f, 0x49, 0xa1, 0x33, 0xa8, 0xf1, 0x30, 0x21, 0x42, 0xba, 0x60, 0x69}),
		"ID":  "1234567",
	}
	errorList = jSchema.Verify(mongoData["_id"].(primitive.ObjectID), mongoData)
	if len(errorList) != 1 || (len(errorList) == 1 && errorList[0].Error() != "document _id: 'ObjectID(\"5f49a133a8f1302142ba6069\")': the value `1234567` (type string  in key `ID`) must be int") {
		for _, err = range errorList {
			t.Error(err)
		}
		t.Fail()
		return
	}
}
