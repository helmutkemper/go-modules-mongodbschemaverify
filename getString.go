package mongoschema

import "errors"

// getString (English): Search for a key with string content within the
// map[string]interface{} and return the value
//
// getString (Português): Procura por uma chave com conteúdo tipo string dentro do
// map[string]interface{} e retorna o valor
func (el *JsonSchema) getString(data map[string]interface{}, keyName string) (keyValue string, found bool, err error) {
	var tmp interface{}

	tmp, found = data[keyName]
	if found == false {
		return
	}

	switch converted := tmp.(type) {
	case string:
		keyValue = converted

	default:
		err = errors.New(KErrorsJSonSchemaBsonTypeString)
	}

	return
}
