package mongoschema

func schemaBool() (schema []byte) {
	return []byte(`{
    "validator" : {
      "$jsonSchema" : {
        "bsonType" : "object",
        "description" : "schema de teste A",
        "additionalProperties": false,
        "required" : [
          "flag"
        ],
        "properties" : {
          "_id": {
            "bsonType": "objectId"
          },
          "flag" : {
            "bsonType" : "bool",
            "description" : "flag obrigatório"
          }
        }
      }
    }
  }`)
}
