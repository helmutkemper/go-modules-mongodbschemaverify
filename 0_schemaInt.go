package mongoschema

func schemaInt() (schema []byte) {
	return []byte(`{
    "validator" : {
      "$jsonSchema" : {
        "bsonType" : "object",
        "description" : "schema de teste A",
        "additionalProperties": false,
        "required" : [
          "ID"
        ],
        "properties" : {
          "_id": {
            "bsonType": "objectId"
          },
          "ID" : {
            "bsonType" : "int",
            "description" : "ID obrigatório"
          }
        }
      }
    }
  }`)
}
