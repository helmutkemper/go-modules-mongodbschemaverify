package mongoschema

//https://docs.mongodb.com/manual/core/schema-validation/#json-schema
func schemaComplex() (schema []byte) {
	return []byte(`{
   "validator": {
      "$jsonSchema": {
         "bsonType": "object",
         "required": [ "name", "year", "major", "address" ],
         "properties": {
            "name": {
               "bsonType": "string",
               "description": "must be a string and is required"
            },
            "year": {
               "bsonType": "int",
               "minimum": 2017,
               "maximum": 3017,
               "description": "must be an integer in [ 2017, 3017 ] and is required"
            },
            "major": {
               "enum": [ "Math", "English", "Computer Science", "History", null ],
               "description": "can only be one of the enum values and is required"
            },
            "gpa": {
               "bsonType": "double",
               "description": "must be a double if the field exists"
            },
            "address": {
               "bsonType": "object",
               "required": [ "city", "street" ],
               "properties": {
                  "street": {
                     "bsonType": "object",
                     "required": [ "name", "number" ],
                     "properties": {
                        "name": {
                           "bsonType": "string",
                           "description": "street name"
                        },
                        "number": {
                           "bsonType": "int",
                           "description": "house number"
                        }
                     }
                  },
                  "city": {
                     "bsonType": "string",
                     "description": "must be a string and is required"
                  }
               }
            }
         }
      }
   }
}`)
}
