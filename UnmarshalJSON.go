package mongoschema

import (
	"encoding/json"
	"errors"
)

// processRequiredFields (English): Process the required fields
//    json schema example:
//    {
//      "bsonType": "object",
//      "title": "<Type Name>",
//      "required": ["<Required Field Name>", ...],
//      "properties": {
//        "<Field Name>": <Schema Document>
//      }
//    }
//
// processRequiredFields (Português): Processa os campos requeridos
//    exemplo de esquema json:
//    {
//      "bsonType": "object",
//      "title": "<Type Name>",
//      "required": ["<Required Field Name>", ...],
//      "properties": {
//        "<Field Name>": <Schema Document>
//      }
//    }
func (el *JsonSchema) processRequiredFields(key string, schema map[string]interface{}) {

	var tmp string
	var foundProperties bool

	for k, v := range schema {
		if k == "required" && v != nil {

			if len(el.RequiredList) == 0 {
				el.RequiredList = make(map[string]bool)
			}

			for _, requiredField := range v.([]interface{}) {

				if key == "" {
					tmp = ""
				} else {
					tmp = key + "."
				}

				el.RequiredList[tmp+requiredField.(string)] = true
				//fmt.Printf("%v%v\n", tmp, requiredField.(string))
			}
		}

		switch converted := v.(type) {
		case map[string]interface{}:

			for k, v := range converted {
				switch converted := v.(type) {
				case map[string]interface{}:
					_, foundProperties = converted["properties"]
					if foundProperties == true {
						if key != "" {
							k = key + "." + k
						}
						el.processRequiredFields(k, converted)
					}

				}
			}

		}
	}
}

func (el *JsonSchema) UnmarshalJSON(data []byte) (err error) {
	var found bool
	var schema = make(map[string]interface{})

	err = json.Unmarshal(data, &schema)
	if err != nil {
		return
	}

	schema, err = el.getJSonMainSchema(schema)
	if err != nil {
		return
	}

	el.processRequiredFields("", schema)

	schema, found = schema[kJsonSchemaProperties].(map[string]interface{})
	if found == false {
		err = errors.New(KErrorsJSonSchemaProperties)
		return
	}

	_, found = schema["_id"].(map[string]interface{})
	if found == false {
		schema["_id"] = map[string]interface{}{
			"bsonType": "objectId",
		}
	}

	// bsonType
	//keyValue, found, err = el.getString(schema, kJsonSchemaBsonType)
	//if err != nil {
	//return
	//}

	//if found == false {
	//	err = errors.New(KErrorsJSonSchemaBSonTypeKey)
	//	return
	//}

	el.Schema = make(map[string]Properties)
	el.processProperties("", schema)

	return
}
