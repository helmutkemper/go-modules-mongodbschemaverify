package mongoschema

func schemaDate() (schema []byte) {
	return []byte(`{
    "validator" : {
      "$jsonSchema" : {
        "bsonType" : "object",
        "description" : "schema de teste A",
        "additionalProperties": false,
        "required" : [
          "DATE"
        ],
        "properties" : {
          "_id": {
            "bsonType": "objectId"
          },
         "DATE" : {
            "bsonType" : "date",
            "description" : "Date de teste"
          }
        }
      }
    }
  }`)
}
