package mongoschema

import (
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"reflect"
	"strings"
)

func (el *JsonSchema) verifyValueIsSet(value interface{}) (valueIsSet bool) {

	valueIsSet = true

	var element = reflect.TypeOf(value)
	var fieldKind = element.Kind()

	switch fieldKind {
	case reflect.Invalid:
		valueIsSet = false

	case reflect.Int:
		if value.(int) == 0 {
			valueIsSet = false
		}

	case reflect.Int8:
		if value.(int8) == 0 {
			valueIsSet = false
		}

	case reflect.Int16:
		if value.(int16) == 0 {
			valueIsSet = false
		}

	case reflect.Int32:
		if value.(int32) == 0 {
			valueIsSet = false
		}

	case reflect.Int64:
		switch value.(type) {
		case int64:
			if value.(int64) == 0 {
				valueIsSet = false
			}
		case primitive.DateTime:
			if value.(primitive.DateTime) == 0 {
				valueIsSet = false
			}
		}

	case reflect.Uint:
		if value.(uint) == 0 {
			valueIsSet = false
		}

	case reflect.Uint8:
		if value.(uint8) == 0 {
			valueIsSet = false
		}

	case reflect.Uint16:
		if value.(uint16) == 0 {
			valueIsSet = false
		}

	case reflect.Uint32:
		if value.(uint32) == 0 {
			valueIsSet = false
		}

	case reflect.Uint64:
		if value.(uint64) == 0 {
			valueIsSet = false
		}

	case reflect.Float32:
		if value.(float32) == 0.0 {
			valueIsSet = false
		}

	case reflect.Float64:
		if value.(float64) == 0.0 {
			valueIsSet = false
		}

	case reflect.Complex64:
		if value.(complex64) == 0 {
			valueIsSet = false
		}

	case reflect.Complex128:
		if value.(complex128) == 0 {
			valueIsSet = false
		}

	case reflect.Array:
		if element.Len() == 0 {
			valueIsSet = false
		}

	case reflect.Map:
		if value == nil {
			valueIsSet = false
		}

	case reflect.Slice:
		if element.Len() == 0 {
			valueIsSet = false
		}

	case reflect.String:
		if value.(string) == "" {
			valueIsSet = false
		}

	case reflect.Struct:
		if element.Len() == 0 {
			valueIsSet = false
		}
	}

	return
}

type NumberEquality int

const (
	KNumberEqualityAIsEqualToB NumberEquality = iota
	KNumberEqualityAIsGreaterThanB
	KNumberEqualityAIsLessThanB
)

func (el *JsonSchema) compareNumbers(a, b interface{}) (equality NumberEquality) {
	var aFloat64 float64
	var bFloat64 float64

	switch converted := a.(type) {
	case uint:
		aFloat64 = float64(converted)
	case uint8:
		aFloat64 = float64(converted)
	case uint16:
		aFloat64 = float64(converted)
	case uint32:
		aFloat64 = float64(converted)
	case uint64:
		aFloat64 = float64(converted)
	case int:
		aFloat64 = float64(converted)
	case int8:
		aFloat64 = float64(converted)
	case int16:
		aFloat64 = float64(converted)
	case int32:
		aFloat64 = float64(converted)
	case int64:
		aFloat64 = float64(converted)
	case float32:
		aFloat64 = float64(converted)
	case float64:
		aFloat64 = converted
	}

	switch converted := b.(type) {
	case uint:
		bFloat64 = float64(converted)
	case uint8:
		bFloat64 = float64(converted)
	case uint16:
		bFloat64 = float64(converted)
	case uint32:
		bFloat64 = float64(converted)
	case uint64:
		bFloat64 = float64(converted)
	case int:
		bFloat64 = float64(converted)
	case int8:
		bFloat64 = float64(converted)
	case int16:
		bFloat64 = float64(converted)
	case int32:
		bFloat64 = float64(converted)
	case int64:
		bFloat64 = float64(converted)
	case float32:
		bFloat64 = float64(converted)
	case float64:
		bFloat64 = converted
	}

	if aFloat64 == bFloat64 {
		return KNumberEqualityAIsEqualToB
	} else if aFloat64 > bFloat64 {
		return KNumberEqualityAIsGreaterThanB
	} else {
		return KNumberEqualityAIsLessThanB
	}
}

func (el *JsonSchema) verifySubData(
	ID primitive.ObjectID,
	key string,
	data map[string]interface{},
) (
	errorList []error,
) {

	errorList = make([]error, 0)

	var found bool
	var properties Properties
	var fieldKind reflect.Kind
	var element reflect.Type

	for fieldName := range el.RequiredList {

		_, properties = el.getRules(fieldName)
		requestList := strings.Split(fieldName, ".")
		var schemaWalking = data
		var value interface{}
		var length = len(requestList) - 1
		for requestKey, requestValue := range requestList {
			if length != requestKey {
				schemaWalking, found = schemaWalking[requestValue].(map[string]interface{})
			} else {
				value, found = schemaWalking[requestValue].(interface{})
			}
		}

		if properties.EnumHasNilValue == false && (found == false || el.verifyValueIsSet(value) == false) {
			errorList = append(errorList, errors.New(fmt.Sprintf("document _id: '%v': field `%v` not found", ID.String(), fieldName)))
		}
	}

	for fieldName, fieldValue := range data {

		if key != "" {
			fieldName = key + "." + fieldName
		}

		if fieldValue == nil {
			fieldKind = reflect.Invalid
		} else {
			element = reflect.TypeOf(fieldValue)
			fieldKind = element.Kind()
		}

		found, properties = el.getRules(fieldName)
		if found == false {
			//fixme: rule not found text
			errorList = append(errorList, properties.getNotFoundErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName))
			continue
		}

		if properties.BsonType == KBsonTypeCustomENum {
			var pass = false
			for _, enumValue := range properties.Enum {
				if enumValue == fieldValue {
					pass = true
					break
				}
			}

			if pass == false {
				errorList = append(errorList, properties.getNotFoundErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName))
			}

			continue
		}

		switch element.Kind() {
		case reflect.Invalid:
		case reflect.Bool:
			if properties.GoType[fieldKind] == true {
				continue
			}

		case reflect.Int:
			if properties.GoType[fieldKind] == true {
				if properties.Min != nil && el.compareNumbers(fieldValue, properties.Min) == KNumberEqualityAIsLessThanB {
					errorList = append(errorList, properties.getLessThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Min))
				}
				if properties.Max != nil && el.compareNumbers(fieldValue, properties.Max) == KNumberEqualityAIsGreaterThanB {
					errorList = append(errorList, properties.getGreaterThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Max))
				}
				continue
			}

		case reflect.Int8:
			if properties.GoType[fieldKind] == true {
				if properties.Min != nil && el.compareNumbers(fieldValue, properties.Min) == KNumberEqualityAIsLessThanB {
					errorList = append(errorList, properties.getLessThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Min))
				}
				if properties.Max != nil && el.compareNumbers(fieldValue, properties.Max) == KNumberEqualityAIsGreaterThanB {
					errorList = append(errorList, properties.getGreaterThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Max))
				}
				continue
			}

		case reflect.Int16:
			if properties.GoType[fieldKind] == true {
				if properties.Min != nil && el.compareNumbers(fieldValue, properties.Min) == KNumberEqualityAIsLessThanB {
					errorList = append(errorList, properties.getLessThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Min))
				}
				if properties.Max != nil && el.compareNumbers(fieldValue, properties.Max) == KNumberEqualityAIsGreaterThanB {
					errorList = append(errorList, properties.getGreaterThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Max))
				}
				continue
			}

		case reflect.Int32:
			if properties.GoType[fieldKind] == true {
				if properties.Min != nil && el.compareNumbers(fieldValue, properties.Min) == KNumberEqualityAIsLessThanB {
					errorList = append(errorList, properties.getLessThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Min))
				}
				if properties.Max != nil && el.compareNumbers(fieldValue, properties.Max) == KNumberEqualityAIsGreaterThanB {
					errorList = append(errorList, properties.getGreaterThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Max))
				}
				continue
			}

		case reflect.Int64:
			if properties.GoType[fieldKind] == true {
				if properties.Min != nil && el.compareNumbers(fieldValue, properties.Min) == KNumberEqualityAIsLessThanB {
					errorList = append(errorList, properties.getLessThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Min))
				}
				if properties.Max != nil && el.compareNumbers(fieldValue, properties.Max) == KNumberEqualityAIsGreaterThanB {
					errorList = append(errorList, properties.getGreaterThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Max))
				}
				continue
			}

		case reflect.Uint:
			if properties.GoType[fieldKind] == true {
				if properties.Min != nil && el.compareNumbers(fieldValue, properties.Min) == KNumberEqualityAIsLessThanB {
					errorList = append(errorList, properties.getLessThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Min))
				}
				if properties.Max != nil && el.compareNumbers(fieldValue, properties.Max) == KNumberEqualityAIsGreaterThanB {
					errorList = append(errorList, properties.getGreaterThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Max))
				}
				continue
			}

		case reflect.Uint8:
			if properties.GoType[fieldKind] == true {
				if properties.Min != nil && el.compareNumbers(fieldValue, properties.Min) == KNumberEqualityAIsLessThanB {
					errorList = append(errorList, properties.getLessThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Min))
				}
				if properties.Max != nil && el.compareNumbers(fieldValue, properties.Max) == KNumberEqualityAIsGreaterThanB {
					errorList = append(errorList, properties.getGreaterThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Max))
				}
				continue
			}

		case reflect.Uint16:
			if properties.GoType[fieldKind] == true {
				if properties.Min != nil && el.compareNumbers(fieldValue, properties.Min) == KNumberEqualityAIsLessThanB {
					errorList = append(errorList, properties.getLessThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Min))
				}
				if properties.Max != nil && el.compareNumbers(fieldValue, properties.Max) == KNumberEqualityAIsGreaterThanB {
					errorList = append(errorList, properties.getGreaterThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Max))
				}
				continue
			}

		case reflect.Uint32:
			if properties.GoType[fieldKind] == true {
				if properties.Min != nil && el.compareNumbers(fieldValue, properties.Min) == KNumberEqualityAIsLessThanB {
					errorList = append(errorList, properties.getLessThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Min))
				}
				if properties.Max != nil && el.compareNumbers(fieldValue, properties.Max) == KNumberEqualityAIsGreaterThanB {
					errorList = append(errorList, properties.getGreaterThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Max))
				}
				continue
			}

		case reflect.Uint64:
			if properties.GoType[fieldKind] == true {
				if properties.Min != nil && el.compareNumbers(fieldValue, properties.Min) == KNumberEqualityAIsLessThanB {
					errorList = append(errorList, properties.getLessThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Min))
				}
				if properties.Max != nil && el.compareNumbers(fieldValue, properties.Max) == KNumberEqualityAIsGreaterThanB {
					errorList = append(errorList, properties.getGreaterThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Max))
				}
				continue
			}

		case reflect.Uintptr:
		case reflect.Float32:
			if properties.GoType[fieldKind] == true {
				if properties.Min != nil && el.compareNumbers(fieldValue, properties.Min) == KNumberEqualityAIsLessThanB {
					errorList = append(errorList, properties.getLessThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Min))
				}
				if properties.Max != nil && el.compareNumbers(fieldValue, properties.Max) == KNumberEqualityAIsGreaterThanB {
					errorList = append(errorList, properties.getGreaterThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Max))
				}
				continue
			}

		case reflect.Float64:
			if properties.GoType[fieldKind] == true {
				if properties.Min != nil && el.compareNumbers(fieldValue, properties.Min) == KNumberEqualityAIsLessThanB {
					errorList = append(errorList, properties.getLessThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Min))
				}
				if properties.Max != nil && el.compareNumbers(fieldValue, properties.Max) == KNumberEqualityAIsGreaterThanB {
					errorList = append(errorList, properties.getGreaterThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Max))
				}
				continue
			}

		case reflect.Complex64:
			if properties.GoType[fieldKind] == true {
				if properties.Min != nil && el.compareNumbers(fieldValue, properties.Min) == KNumberEqualityAIsLessThanB {
					errorList = append(errorList, properties.getLessThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Min))
				}
				if properties.Max != nil && el.compareNumbers(fieldValue, properties.Max) == KNumberEqualityAIsGreaterThanB {
					errorList = append(errorList, properties.getGreaterThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Max))
				}
				continue
			}

		case reflect.Complex128:
			if properties.GoType[fieldKind] == true {
				if properties.Min != nil && el.compareNumbers(fieldValue, properties.Min) == KNumberEqualityAIsLessThanB {
					errorList = append(errorList, properties.getLessThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Min))
				}
				if properties.Max != nil && el.compareNumbers(fieldValue, properties.Max) == KNumberEqualityAIsGreaterThanB {
					errorList = append(errorList, properties.getGreaterThanErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName, fieldValue, properties.Max))
				}
				continue
			}

		case reflect.Array:
			if properties.GoType[fieldKind] == true {
				continue
			}

		case reflect.Chan:
		case reflect.Func:
		case reflect.Interface:
			if properties.GoType[fieldKind] == true {
				continue
			}

		case reflect.Map:
			//el.processRequiredFields(fieldName, fieldValue.(map[string]interface{}))
			el.verifySubData(ID, key+"."+fieldName, fieldValue.(map[string]interface{}))
			if properties.GoType[fieldKind] == true {
				continue
			}

		case reflect.Ptr:
		case reflect.Slice:
			if pa, ok := fieldValue.(primitive.A); ok {
				valueMSI := []interface{}(pa)
				for _, v := range valueMSI {
					//el.processRequiredFields(fieldName, v.(map[string]interface{}))
					el.verifySubData(ID, key+"."+fieldName, v.(map[string]interface{}))
				}
			}

			if properties.GoType[fieldKind] == true {
				continue
			}

		case reflect.String:
			if properties.GoType[fieldKind] == true {
				continue
			}

		case reflect.Struct:
			//el.processRequiredFields(fieldName, fieldValue.(map[string]interface{}))
			el.verifySubData(ID, key+"."+fieldName, fieldValue.(map[string]interface{}))
			if properties.GoType[fieldKind] == true {
				continue
			}

		case reflect.UnsafePointer:
		}

		errorList = append(errorList, properties.getNotFoundErrorString(ID.String(), fieldValue, fieldKind.String(), fieldName))
	}

	return
}
