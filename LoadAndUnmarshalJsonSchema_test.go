package mongoschema

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"time"
)

func ExampleJsonSchema_LoadAndUnmarshalJsonSchema() {
	var err error
	var errList = make([]error, 0)

	var data = []map[string]interface{}{
		{
			"_id":    primitive.ObjectID([12]byte{0x5f, 0x49, 0xa1, 0x33, 0xa8, 0xf1, 0x30, 0x21, 0x42, 0xba, 0x60, 0x69}),
			"int":    1234567,
			"double": 3.1415,
			"date":   primitive.NewDateTimeFromTime(time.Now()),
		},
	}

	var v = JsonSchema{}
	v.AcceptsIntegerAsDouble()
	err = v.LoadAndUnmarshalJsonSchema("./0_schema.json")
	if err != nil {
		panic(err)
	}

	for _, mData := range data {
		errList = v.Verify(mData["_id"].(primitive.ObjectID), mData)
		for _, err := range errList {
			log.Printf("%v\n", err)
		}
	}

	// Output:
	//
}
