package mongoschema

func schemaDouble() (schema []byte) {
	return []byte(`{
    "validator" : {
      "$jsonSchema" : {
        "bsonType" : "object",
        "description" : "schema de teste A",
        "additionalProperties": false,
        "required" : [
          "DOUBLE"
        ],
        "properties" : {
          "_id": {
            "bsonType": "objectId"
          },
          "DOUBLE" : {
            "bsonType" : "double",
            "description" : "DOUBLE obrigatório"
          }
        }
      }
    }
  }`)
}
