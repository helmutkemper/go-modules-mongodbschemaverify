package mongoschema

func schemaString() (schema []byte) {
	return []byte(`{
    "validator" : {
      "$jsonSchema" : {
        "bsonType" : "object",
        "description" : "schema de teste A",
        "additionalProperties": false,
        "required" : [
          "text"
        ],
        "properties" : {
          "_id": {
            "bsonType": "objectId"
          },
          "text" : {
            "bsonType" : "string",
            "description" : "texto é obrigatório"
          }
        }
      }
    }
  }`)
}
