package mongoschema

import (
	"reflect"
)

// getJSonSchema (English):
//
// getJSonSchema (Português):
func (el *JsonSchema) getJSonSchema(prefix string, key string, data map[string]interface{}) {
	var found bool
	var description string
	var properties = make(map[string]interface{})
	var enum []interface{}
	var enumHasNilValue bool

	if prefix != "" {
		key = prefix + "." + key
	}

	enum, found = data[kJsonSchemaEnum].([]interface{})
	if found == false {
		enum = make([]interface{}, 0)
	}

	//fixme: data[kJsonSchemaBsonType] pode ser []interface{}
	switch data[kJsonSchemaBsonType] {
	case nil:

		if len(enum) != 0 {
			for _, v := range enum {
				if v == nil {
					enumHasNilValue = true
					break
				}
			}

			description, _ = data[kJsonSchemaDescription].(string)
			_, found = el.RequiredList[key]
			el.Schema[key] = Properties{
				Key:                  key,
				Enum:                 enum,
				EnumHasNilValue:      enumHasNilValue,
				BsonType:             KBsonTypeCustomENum,
				GoType:               nil,
				Description:          description,
				AdditionalProperties: nil,
				Required:             found,
				ErrorNotFound:        "document _id: '%v': the value `%v` (type %v  in key `%v`) does not match the values contained in the enumerator",
			}
		}

	case "double":
		description, _ = data[kJsonSchemaDescription].(string)
		_, found = el.RequiredList[key]
		el.Schema[key] = Properties{
			Key:      key,
			Enum:     enum,
			BsonType: KBsonTypeDouble,
			GoType: map[reflect.Kind]bool{
				reflect.Float32: true,
				reflect.Float64: true,
			},
			Description:          description,
			AdditionalProperties: nil,
			Required:             found,
			ErrorNotFound:        "document _id: '%v': the value `%v` (type %v  in key `%v`) must be double",
			ErrorMinValue:        "document _id: '%v': the value `%v` (type %v  in key `%v`) %v must be greater than %v",
			ErrorMaxValue:        "document _id: '%v': the value `%v` (type %v  in key `%v`) %v must be less than %v",
			Min:                  data[kJsonSchemaMinimum],
			Max:                  data[kJsonSchemaMaximum],
		}
		if el.IgnoreDoubleAsInt == true {
			var properties = el.Schema[key]
			properties.GoType[reflect.Int] = true
			properties.GoType[reflect.Int8] = true
			properties.GoType[reflect.Int16] = true
			properties.GoType[reflect.Int32] = true
			properties.GoType[reflect.Int64] = true

			el.Schema[key] = properties
		}

	case "string":
		description, _ = data[kJsonSchemaDescription].(string)
		_, found = el.RequiredList[key]
		el.Schema[key] = Properties{
			Key:      key,
			Enum:     enum,
			BsonType: KBsonTypeString,
			GoType: map[reflect.Kind]bool{
				reflect.String: true,
			},
			Description:          description,
			AdditionalProperties: nil,
			Required:             found,
			ErrorNotFound:        "document _id: '%v': the value `%v` (type %v  in key `%v`) must be string",
		}

	case "object":
		description, _ = data[kJsonSchemaDescription].(string)
		_, found = el.RequiredList[key]
		el.Schema[key] = Properties{
			Key:      key,
			Enum:     enum,
			BsonType: KBsonTypeObject,
			GoType: map[reflect.Kind]bool{
				reflect.Struct: true,
				reflect.Map:    true,
			},
			Description:          description,
			AdditionalProperties: nil,
			Required:             found,
			ErrorNotFound:        "document _id: '%v': the value `%v` (type %v  in key `%v`) must be object",
		}

		properties, found = data[kJsonSchemaProperties].(map[string]interface{})
		el.processProperties(key, properties)

	case "array":
		description, _ = data[kJsonSchemaDescription].(string)
		_, found = el.RequiredList[key]
		el.Schema[key] = Properties{
			Key:      key,
			Enum:     enum,
			BsonType: KBsonTypeObject,
			GoType: map[reflect.Kind]bool{
				reflect.Slice: true,
			},
			Description:          description,
			AdditionalProperties: nil,
			Required:             found,
			ErrorNotFound:        "document _id: '%v': the value `%v` (type %v  in key `%v`) must be array",
		}

		properties, found = data[kJsonSchemaProperties].(map[string]interface{})
		el.processProperties(key, properties)

	case "binData":
		description, _ = data[kJsonSchemaDescription].(string)
		_, found = el.RequiredList[key]
		el.Schema[key] = Properties{
			Key:      key,
			Enum:     enum,
			BsonType: KBsonTypeBinaryData,
			GoType: map[reflect.Kind]bool{
				reflect.Slice: true,
			},
			Description:          description,
			AdditionalProperties: nil,
			Required:             found,
			ErrorNotFound:        "document _id: '%v': the value `%v` (type %v  in key `%v`) must be binData",
		}

	case "undefined":
	case "objectId":
		description, _ = data[kJsonSchemaDescription].(string)
		_, found = el.RequiredList[key]
		el.Schema[key] = Properties{
			Key:      key,
			Enum:     enum,
			BsonType: KBsonTypeObjectId,
			GoType: map[reflect.Kind]bool{
				reflect.Array: true,
			},
			Description:          description,
			AdditionalProperties: nil,
			Required:             found,
			ErrorNotFound:        "document _id: '%v': the value `%v` (type %v  in key `%v`) must be objectId",
		}

	case "bool":
		description, _ = data[kJsonSchemaDescription].(string)
		_, found = el.RequiredList[key]
		el.Schema[key] = Properties{
			Key:      key,
			Enum:     enum,
			BsonType: KBsonTypeBoolean,
			GoType: map[reflect.Kind]bool{
				reflect.Bool: true,
			},
			Description:          description,
			AdditionalProperties: nil,
			Required:             found,
			ErrorNotFound:        "document _id: '%v': the value `%v` (type %v  in key `%v`) must be bool",
		}

	case "date":
		description, _ = data[kJsonSchemaDescription].(string)
		_, found = el.RequiredList[key]
		el.Schema[key] = Properties{
			Key:      key,
			Enum:     enum,
			BsonType: KBsonTypeDate,
			GoType: map[reflect.Kind]bool{
				reflect.Int:   true,
				reflect.Int64: true,
			},
			Description:          description,
			AdditionalProperties: nil,
			Required:             found,
			ErrorNotFound:        "document _id: '%v': the value `%v` (type %v  in key `%v`) must be date",
			ErrorMinValue:        "document _id: '%v': the value `%v` (type %v  in key `%v`) %v must be greater than %v",
			ErrorMaxValue:        "document _id: '%v': the value `%v` (type %v  in key `%v`) %v must be less than %v",
			//fixme: min e max for date
		}

	case "null":
		description, _ = data[kJsonSchemaDescription].(string)
		_, found = el.RequiredList[key]
		el.Schema[key] = Properties{
			Key:      key,
			Enum:     enum,
			BsonType: KBsonTypeNull,
			GoType: map[reflect.Kind]bool{
				reflect.Invalid: true,
			},
			Description:          description,
			AdditionalProperties: nil,
			Required:             found,
			ErrorNotFound:        "document _id: '%v': the value `%v` (type %v  in key `%v`) must be null",
		}

	case "regex":
	case "dbPointer":
	case "javascript":
	case "symbol":
	case "javascriptWithScope":
	case "int":
		description, _ = data[kJsonSchemaDescription].(string)
		_, found = el.RequiredList[key]
		el.Schema[key] = Properties{
			Key:      key,
			Enum:     enum,
			BsonType: KBsonTypeInteger32bit,
			GoType: map[reflect.Kind]bool{
				reflect.Int:   true,
				reflect.Int32: true,
				reflect.Int64: true,
			},
			Description:          description,
			AdditionalProperties: nil,
			Required:             found,
			ErrorNotFound:        "document _id: '%v': the value `%v` (type %v  in key `%v`) must be int",
			ErrorMinValue:        "document _id: '%v': the value `%v` (type %v  in key `%v`) %v must be greater than %v",
			ErrorMaxValue:        "document _id: '%v': the value `%v` (type %v  in key `%v`) %v must be less than %v",
			Min:                  data[kJsonSchemaMinimum],
			Max:                  data[kJsonSchemaMaximum],
		}

	case "timestamp":
		description, _ = data[kJsonSchemaDescription].(string)
		_, found = el.RequiredList[key]
		el.Schema[key] = Properties{
			Key:      key,
			Enum:     enum,
			BsonType: KBsonTypeTimestamp,
			GoType: map[reflect.Kind]bool{
				reflect.Int64: true,
			},
			Description:          description,
			AdditionalProperties: nil,
			Required:             found,
			ErrorNotFound:        "document _id: '%v': the value `%v` (type %v  in key `%v`) must be timestamp",
			ErrorMinValue:        "document _id: '%v': the value `%v` (type %v  in key `%v`) %v must be greater than %v",
			ErrorMaxValue:        "document _id: '%v': the value `%v` (type %v  in key `%v`) %v must be less than %v",
			//fixme: min e max for timestamp
		}

	case "long":
		description, _ = data[kJsonSchemaDescription].(string)
		_, found = el.RequiredList[key]
		el.Schema[key] = Properties{
			Key:      key,
			BsonType: KBsonTypeInteger64bit,
			GoType: map[reflect.Kind]bool{
				reflect.Int64: true,
			},
			Description:          description,
			AdditionalProperties: nil,
			Required:             found,
			ErrorNotFound:        "document _id: '%v': the value `%v` (type %v  in key `%v`) must be long",
			ErrorMinValue:        "document _id: '%v': the value `%v` (type %v  in key `%v`) %v must be greater than %v",
			ErrorMaxValue:        "document _id: '%v': the value `%v` (type %v  in key `%v`) %v must be less than %v",
			Min:                  data[kJsonSchemaMinimum],
			Max:                  data[kJsonSchemaMaximum],
		}

	case "decimal":
		description, _ = data[kJsonSchemaDescription].(string)
		_, found = el.RequiredList[key]
		el.Schema[key] = Properties{
			Key:      key,
			Enum:     enum,
			BsonType: KBsonTypeDecimal128,
			GoType: map[reflect.Kind]bool{
				reflect.Float32: true,
				reflect.Float64: true,
			},
			Description:          description,
			AdditionalProperties: nil,
			Required:             found,
			ErrorNotFound:        "document _id: '%v': the value `%v` (type %v  in key `%v`) must be decimal",
			ErrorMinValue:        "document _id: '%v': the value `%v` (type %v  in key `%v`) %v must be greater than %v",
			ErrorMaxValue:        "document _id: '%v': the value `%v` (type %v  in key `%v`) %v must be less than %v",
			Min:                  data[kJsonSchemaMinimum],
			Max:                  data[kJsonSchemaMaximum],
		}
	}
}
