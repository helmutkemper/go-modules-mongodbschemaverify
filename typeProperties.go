package mongoschema

import (
	"errors"
	"fmt"
	"reflect"
)

type Properties struct {
	Key                  string
	Type                 string
	Enum                 []interface{}
	EnumHasNilValue      bool
	BsonType             BsonType
	GoType               map[reflect.Kind]bool
	Description          string
	AdditionalProperties AdditionalProperties
	Required             bool
	ErrorNotFound        string
	ErrorMinValue        string
	ErrorMaxValue        string
	Min                  interface{}
	Max                  interface{}
}

func (el Properties) getNotFoundErrorString(a ...interface{}) (err error) {
	return errors.New(fmt.Sprintf(el.ErrorNotFound, a[0], a[1], a[2], a[3]))
}

//ID.String(), fieldValue, fieldKind.String(), fieldName
func (el Properties) getGreaterThanErrorString(a ...interface{}) (err error) {
	return errors.New(fmt.Sprintf(el.ErrorMaxValue, a[0], a[1], a[2], a[3], a[1], a[5]))
}

func (el Properties) getLessThanErrorString(a ...interface{}) (err error) {
	return errors.New(fmt.Sprintf(el.ErrorMinValue, a[0], a[1], a[2], a[3], a[1], a[5]))
}
