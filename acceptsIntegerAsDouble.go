package mongoschema

// AcceptsIntegerAsDouble (English): Ignore integer for double type fields
//
// AcceptsIntegerAsDouble (Português): Ignora inteiro para campos tipo double
func (el *JsonSchema) AcceptsIntegerAsDouble() {
	el.IgnoreDoubleAsInt = true
}
