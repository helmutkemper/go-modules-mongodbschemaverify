package mongoschema

type JsonSchema struct {
	//  (English): Receive schema keys with schema rules
	//  (Português): Recebe as chaves do esquema com as regras do esquema
	Schema map[string]Properties

	//  (English): Receives the list of mandatory keys in the data
	//  (Português): Recebe a lista de chaves obrigatórias no dado
	RequiredList map[string]bool

	//  (English): true = allows an integer type value in place of a floating point type
	//  (Português): true - permite um valor do tipo inteiro no lugar de um tipo ponto
	//  flutuante
	IgnoreDoubleAsInt bool
}
