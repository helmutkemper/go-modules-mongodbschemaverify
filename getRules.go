package mongoschema

// getRules (English): Returns the specific rule for the key contained in the MongoDB
// data
//
// getRules (Português): Retorna a regra específica da chave contida no dado do MongoDB
func (el *JsonSchema) getRules(key string) (found bool, properties Properties) {
	properties, found = el.Schema[key]
	return
}
