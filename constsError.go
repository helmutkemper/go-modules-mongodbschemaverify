package mongoschema

const (
	KErrorsJSonSchemaValidator      = "validator must be a map[string]interface{}"
	KErrorsJSonSchemaProperties     = "schema properties key not found"
	KErrorsJSonSchemaBSonTypeKey    = "schema properties bsonType key not found"
	KErrorsJSonSchemaBsonTypeString = "bsonType key must be a string"
	//KErrorsJSonSchemaArrayString       = "array string not found"
	//KErrorsJSonSchemaDescriptionString = "description key must be a string"
	KErrorsJSonSchemaKeyNotFoundInSchema = "key '%v' not found in mongo schema"
	KErrorsJSonSchemaKeyMustBeBool       = "key '%v' must be bool"
	KErrorsJSonSchemaKeyMustBeInt        = "key '%v' must be integer"
)
