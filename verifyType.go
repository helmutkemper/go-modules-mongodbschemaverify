package mongoschema

import "errors"

func (el *JsonSchema) verifyType(data interface{}) (schemaData map[string]interface{}, err error) {
	switch converted := data.(type) {
	case map[string]interface{}:
		schemaData = converted
		return

	default:
		err = errors.New(KErrorsJSonSchemaValidator)
		return
	}
}
