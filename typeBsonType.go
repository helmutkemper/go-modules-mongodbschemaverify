package mongoschema

/*
 https://docs.mongodb.com/realm/mongodb/document-schemas/

 https://docs.mongodb.com/manual/reference/bson-types/

   | Type                    | Number | Alias                 | Notes
   |-------------------------|--------|-----------------------|------------------------
   | Double                  | 1      | “double”              |
   | String                  | 2      | “string”              |
   | Object                  | 3      | “object”              |
   | Array                   | 4      | “array”               |
   | Binary data             | 5      | “binData”             |
   | Undefined               | 6      | “undefined”           | Deprecated.
   | ObjectId                | 7      | “objectId”            |
   | Boolean                 | 8      | “bool”                |
   | Date                    | 9      | “date”                |
   | Null                    | 10     | “null”                |
   | Regular Expression      | 11     | “regex”               |
   | DBPointer               | 12     | “dbPointer”           | Deprecated.
   | JavaScript              | 13     | “javascript”          |
   | Symbol                  | 14     | “symbol”              | Deprecated.
   | JavaScript (with scope) | 15     | “javascriptWithScope” | Deprecated in MongoDB 4.4.
   | 32-bit integer          | 16     | “int”                 |
   | Timestamp               | 17     | “timestamp”           |
   | 64-bit integer          | 18     | “long”                |
   | Decimal128              | 19     | “decimal”             | New in version 3.4.
   | Min key                 | -1     | “minKey”              |
   | Max key                 | 127    | “maxKey”              |
*/
type BsonType int

func (el BsonType) String() (alias string) {
	if el == KBsonTypeMinKey {
		return "minKey"
	}

	if el == KBsonTypeMaxKey {
		return "maxKey"
	}

	return bsonTypeAsStringList[el]
}

func (el BsonType) FromAlias(alias string) (Type BsonType) {
	switch alias {
	case "":
	case "double":
		Type = KBsonTypeDouble
	case "string":
		Type = KBsonTypeString
	case "object":
		Type = KBsonTypeObject
	case "array":
		Type = KBsonTypeArray
	case "binData":
		Type = KBsonTypeBinaryData
	case "undefined":
		Type = KBsonTypeUndefined
	case "objectId":
		Type = KBsonTypeObjectId
	case "bool":
		Type = KBsonTypeBoolean
	case "date":
		Type = KBsonTypeDate
	case "null":
		Type = KBsonTypeNull
	case "regex":
		Type = KBsonTypeRegularExpression
	case "dbPointer":
		Type = KBsonTypeDBPointer
	case "javascript":
		Type = KBsonTypeJavaScript
	case "symbol":
		Type = KBsonTypeSymbol
	case "javascriptWithScope":
		Type = KBsonTypeJavaScriptWithScope
	case "int":
		Type = KBsonTypeInteger32bit
	case "timestamp":
		Type = KBsonTypeTimestamp
	case "long":
		Type = KBsonTypeInteger64bit
	case "decimal":
		Type = KBsonTypeDecimal128
	case "minKey":
		Type = KBsonTypeMinKey
	case "maxKey":
		Type = KBsonTypeMaxKey
	}

	return
}

const (
	KBsonTypeMinKey              BsonType = -1
	KBsonTypeMaxKey              BsonType = 127
	KBsonTypeDouble              BsonType = iota - 1
	KBsonTypeString                       //
	KBsonTypeObject                       //
	KBsonTypeArray                        //
	KBsonTypeBinaryData                   //fixme: tem no banco?
	KBsonTypeUndefined                    //fixme: tem no banco?
	KBsonTypeObjectId                     //todo: fazer
	KBsonTypeBoolean                      //
	KBsonTypeDate                         //
	KBsonTypeNull                         //fixme: tem no banco?
	KBsonTypeRegularExpression            //fixme: tem no banco?
	KBsonTypeDBPointer                    //fixme: tem no banco?
	KBsonTypeJavaScript                   //todo: fazer fixme: tem no banco?
	KBsonTypeSymbol                       //fixme: tem no banco?
	KBsonTypeJavaScriptWithScope          //todo: fazer fixme: tem no banco?
	KBsonTypeInteger32bit                 //
	KBsonTypeTimestamp                    //todo: fazer
	KBsonTypeInteger64bit                 //
	KBsonTypeDecimal128                   //????
	KBsonTypeCustomENum
)

var bsonTypeAsStringList = [...]string{
	"",
	"double",
	"string",
	"object",
	"array",
	"binData",
	"undefined",
	"objectId",
	"bool",
	"date",
	"null",
	"regex",
	"dbPointer",
	"javascript",
	"symbol",
	"javascriptWithScope",
	"int",
	"timestamp",
	"long",
	"decimal",
}
